from django.urls import  path

from vktelegram import views


urlpatterns = [
    path('account/',views.autorize, name='autorize'),
    path('get_grupp/', views.get_grupp, name='get_grupp'),
    path('lead/<int:id>/', views.lead, name='lead'),
    path('forms_data/<int:group_id>/<int:form_id>/',
         views.forms_data, name='forms_data'),
    path('start/', views.start, name='start'),
    path('stop/', views.stop, name='stop'),
    #path('telegram_name/', views.telegram, name='telegram'),
]