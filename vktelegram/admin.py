from django.contrib import admin
from vktelegram.models import Telegramautorize, Vkaccesstoken, Vkgroups, Vkforms


class TelegramautorizeAdmin(admin.ModelAdmin):
    list_display = ('id', 'chat_id')


class VkaccesstokenAdmin (admin.ModelAdmin):
    list_display = ('id', 'vk_access_token')


class VkgroupsAdmin (admin.ModelAdmin):
    list_display = ('id', 'id_group', 'name_group')


class VkformsAdmin (admin.ModelAdmin):
    list_display = ('id', 'id_form', 'id_groupform')


admin.site.register(Telegramautorize)
admin.site.register(Vkaccesstoken)
admin.site.register(Vkgroups)
admin.site.register(Vkforms)