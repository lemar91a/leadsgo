from django.db import models


class Telegramautorize (models.Model):
    chat_id = models.PositiveIntegerField(null=True)


class Vkaccesstoken (models.Model):
    vk_access_token = models.TextField(null=True)


class Vkgroups (models.Model):
    id_group = models.PositiveIntegerField(null=True)
    name_group = models.TextField()


class Vkforms (models.Model):
    id_form = models.PositiveIntegerField(null=True)
    id_groupform = models.PositiveIntegerField(null=True)