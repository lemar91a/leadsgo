from django.shortcuts import render
from django.shortcuts import redirect
import requests
import threading
import time

import psycopg2

from .models import Telegramautorize, Vkaccesstoken, Vkgroups, Vkforms


def landing(request):
    return render(request, 'landing.html')


def main(request):
    return render(request, 'vktelegram/main.html')


def autorize(request):
    """ OAuth авторизация ВКонтакте, получение токена и запись токена в базу"""
    url_code = request.get_full_path()
    code_1 = url_code.split('?')[1].split('=')[1]
    rem = requests.get(
        'https://oauth.vk.com/access_token',
        params={
            'client_id': '7520963',
            'client_secret': 'eWH2hvINTOY4VFTg7tBp',
            'redirect_uri': 'http://localhost:8000/autorize/account',
            'code': code_1
                },
    )

    s = rem.json()
    access_token = str(s.get('access_token'))
    print(access_token)

    conn = psycopg2.connect(dbname='leadsgodb', user='leadsgouser')
    c = conn.cursor()
    c.execute("INSERT INTO vktelegram_vkaccesstoken(vk_access_token) \
              VALUES (%(vk_access_token)s)",
              {'vk_access_token': access_token}
    )
    conn.commit()
    conn.close()

    return render(request,
                  'vktelegram/autorize.html',
                  context={"code": code_1, "res": access_token})


def get_grupp(request):
    """Получение доступных групп ВК и запись в базу"""
    a_tok3 = Vkaccesstoken.objects.order_by('-id')[0].vk_access_token
    print(a_tok3)
    retarget = requests.get(
        'https://api.vk.com/method/users.get',
        params={
            'access_token': a_tok3,
            'v': '5.110'
        },
    )
    spisok_4 = retarget.json()
    id_account = spisok_4["response"][0]['id']

    retarget_2 = requests.get(
        'https://api.vk.com/method/groups.get',
        params={
            'user_id': id_account,
            'extended': '1',
            'filter': 'admin',
            'fields': 'name',
            'access_token': a_tok3,
            'v': '5.110'
        },
    )
    spisok_4 = retarget_2.json()
    groups = spisok_4["response"]['items']
    groups_info = []
    for gr in groups:
        group_info = []
        group_info.append(gr['id'])
        group_info.append(gr['name'])

        groups_info.append(group_info)
    print(groups_info)
    for gp in groups_info:
        if not Vkgroups.objects.filter(name_group=gp[1]).exists():
            conn = psycopg2.connect(dbname='leadsgodb', user='leadsgouser')
            b = conn.cursor()
            query = "INSERT INTO vktelegram_vkgroups(id_group, name_group)\
                    VALUES (%s, %s);"
            b.executemany(query, groups_info)
            conn.commit()
            conn.close()

    return render(request,
                  'vktelegram/autorize.html',
                  context={"spisok_group": groups})


def lead(request, id):
    """Получение лид-форм выбранной группы и запись в базу лид-форм"""
    a_tok4 = Vkaccesstoken.objects.order_by('-id')[0].vk_access_token
    group = Vkgroups.objects.get(id_group=id).id_group
    print(group)
    retarget = requests.get(
        'https://api.vk.com/method/leadForms.list',
        params={
            'group_id': group,
            'access_token': a_tok4,
            'v': '5.110'
        },
    )
    spisok_5 = retarget.json()
    forms =  spisok_5["response"]
    print(forms)

    leads_info = []
    for fr in forms:
        lead_info = []
        lead_info.append(fr['form_id'])
        lead_info.append(fr['group_id'])

        leads_info.append(lead_info)
    print(leads_info)
    for fm in leads_info:
        if not Vkforms.objects.filter(id_form=fm[0]).exists():
            conn = psycopg2.connect(dbname='leadsgodb', user='leadsgouser')
            c = conn.cursor()
            query = "INSERT INTO vktelegram_vkforms(id_form, id_groupform)\
                    VALUES (%s, %s);"
            c.executemany(query, leads_info)
            conn.commit()
            conn.close()

    return render(request,
                  'vktelegram/autorize.html',
                  context={"lead_forms": forms})


def forms_data(request, group_id, form_id):
    """Выбор конкретной лид-формы"""
    groupform_id = Vkforms.objects.get(id_form=form_id).id_form
    global f_id
    f_id = groupform_id

    gform_id = Vkgroups.objects.get(id_group=group_id).id_group
    global g_id
    g_id = gform_id
    print(f_id)
    print(g_id)
    return render(request, 'vktelegram/autorize.html')


def start(*args,**kwargs):
    """Запрос на получение всех лидов выбранной ли-формы, фильтрация
    и отправка в Telegram только новых пришедших лидов"""
    a_tok5 = Vkaccesstoken.objects.order_by('-id')[0].vk_access_token
    print(a_tok5)
    retarget = requests.get(
        'https://api.vk.com/method/leadForms.getLeads',
        params={
            'group_id': g_id,
            'form_id': f_id,
            'access_token': a_tok5,
            'v': '5.110'
        },
    )
    spisok_6 = retarget.json()
    print(spisok_6)
    data =  spisok_6["response"]["leads"]


    all_info = []
    for member in data:
        member_info = []
        #Проверка свежести лида
        if int(time.time()) - int(member['date']) < 20:
            member_info.append(member['lead_id'])

            answers = member['answers']
            for answer in answers:
                answer_key = answer.get('key', None)

                if answer_key == 'phone_number':
                    member_info.insert(1, answer['answer']['value'])

                elif answer_key == 'email':
                    member_info.insert(2, answer['answer']['value'])

                elif answer_key == 'first_name':
                    member_info.insert(3, answer['answer']['value'])

            all_info.append(member_info)

    print(all_info)

    chat_s = Telegramautorize.objects.order_by('-id')[0].chat_id
    print(chat_s)

    if len(all_info) > 0:
        for i in all_info:
            info_lead = 'Имя: {2},\nТелефон: {1},\nE-Mail: {3}'
            info_lead_format = info_lead.format(*i)
            print(info_lead_format)
            print(type(info_lead_format))
            telegram_send = requests.get(
                'https://api.telegram.org/bot1353511770:'
                'AAHm7bMqf3g6ioCab6FegIBrlObDMuRSbTE/sendMessage',
                params={
                    'chat_id': chat_s,
                    'text': info_lead_format,
                },)

    global period
    period = threading.Timer(20, start)
    period.start()
    return redirect('http://localhost:8000/autorize/get_grupp/')


def stop (*args,**kwargs):
    """Остановка процесса передачи лидов"""
    period.cancel()
    return redirect('http://localhost:8000/autorize/get_grupp/')


def telegram(request):
    """Авторизация в Telegram и запись данных в базу"""
    if 'username' in request.GET:
        global username
        username = request.GET['username']
    telegram_getupdates= requests.get('https://api.telegram.org/bot1353511770:'
                                      'AAHm7bMqf3g6ioCab6FegIBrlObDMuRSbTE/'
                                      'getUpdates'
    )
    getupdates_all = telegram_getupdates.json()
    getupdates = getupdates_all['result']

    all_info = []
    for member in getupdates:
        member_info = []
        member_info.append(member['update_id'])
        from_tel = member['message']['from']
        for k, v in from_tel.items():
            member_info.append(v)

        all_info.append(member_info)
    print(all_info)

    for mem in all_info:
        if username in mem:
            global chat_id
            chat_id = mem[1]
    print(chat_id)

    conn = psycopg2.connect(dbname='leadsgodb', user='leadsgouser')
    c = conn.cursor()
    c.execute("INSERT INTO vktelegram_telegramautorize(chat_id)\
              VALUES (%(chat_id)s)",
              {'chat_id': chat_id})
    conn.commit()
    conn.close()

    return render(request,
                  'vktelegram/autorize.html',
                  context={"username": username,
                           "chat_id": chat_id})

