Django==2.2.12
oauthlib==3.1.0
Pillow==7.0.0
pipenv==11.9.0
psycopg2-binary==2.8.6
sqlparse==0.2.4
